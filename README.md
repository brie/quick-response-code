# Quick Response Code

This project contains a `.gitlab-ci.yml` file that lets you:

  - Create a QR code that links to the **Pipelines** page for the GitLab CI pipeline that created the QR code
  - Deploys a website to GitLab Pages that contains the QR code

See [demo](https://brie.gitlab.io/quick-response-code/).

## Usage
This is currently a work in progress. When complete, you should be able to include this file as `quick-response-code.yml` and use it in your existing pipelines in GitLab. 